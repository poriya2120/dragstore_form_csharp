﻿namespace darokhane
{
    partial class drag_g
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(drag_g));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dragIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dragNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dragSEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dragGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dragGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.darokhaneDataSet = new darokhane.darokhaneDataSet();
            this.dragGTableAdapter = new darokhane.darokhaneDataSetTableAdapters.dragGTableAdapter();
            this.button1 = new System.Windows.Forms.Button();
            this.darokhaneDataSet1 = new darokhane.darokhaneDataSet1();
            this.dragGBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dragGTableAdapter1 = new darokhane.darokhaneDataSet1TableAdapters.dragGTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.darokhaneDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.darokhaneDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragGBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dragIdDataGridViewTextBoxColumn,
            this.dragNameDataGridViewTextBoxColumn,
            this.dragSEDataGridViewTextBoxColumn,
            this.dragGDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.dragGBindingSource1;
            this.dataGridView1.GridColor = System.Drawing.Color.Lime;
            this.dataGridView1.Location = new System.Drawing.Point(101, 77);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(894, 430);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dragIdDataGridViewTextBoxColumn
            // 
            this.dragIdDataGridViewTextBoxColumn.DataPropertyName = "dragId";
            this.dragIdDataGridViewTextBoxColumn.FillWeight = 150F;
            this.dragIdDataGridViewTextBoxColumn.HeaderText = "id";
            this.dragIdDataGridViewTextBoxColumn.Name = "dragIdDataGridViewTextBoxColumn";
            this.dragIdDataGridViewTextBoxColumn.Width = 150;
            // 
            // dragNameDataGridViewTextBoxColumn
            // 
            this.dragNameDataGridViewTextBoxColumn.DataPropertyName = "dragName";
            this.dragNameDataGridViewTextBoxColumn.HeaderText = "نام دارو";
            this.dragNameDataGridViewTextBoxColumn.Name = "dragNameDataGridViewTextBoxColumn";
            this.dragNameDataGridViewTextBoxColumn.Width = 150;
            // 
            // dragSEDataGridViewTextBoxColumn
            // 
            this.dragSEDataGridViewTextBoxColumn.DataPropertyName = "dragSE";
            this.dragSEDataGridViewTextBoxColumn.HeaderText = "طبقه بندی";
            this.dragSEDataGridViewTextBoxColumn.Name = "dragSEDataGridViewTextBoxColumn";
            this.dragSEDataGridViewTextBoxColumn.Width = 150;
            // 
            // dragGDataGridViewTextBoxColumn
            // 
            this.dragGDataGridViewTextBoxColumn.DataPropertyName = "dragG";
            this.dragGDataGridViewTextBoxColumn.HeaderText = "محتوا";
            this.dragGDataGridViewTextBoxColumn.Name = "dragGDataGridViewTextBoxColumn";
            this.dragGDataGridViewTextBoxColumn.Width = 150;
            // 
            // dragGBindingSource
            // 
            this.dragGBindingSource.DataMember = "dragG";
            this.dragGBindingSource.DataSource = this.darokhaneDataSet;
            // 
            // darokhaneDataSet
            // 
            this.darokhaneDataSet.DataSetName = "darokhaneDataSet";
            this.darokhaneDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dragGTableAdapter
            // 
            this.dragGTableAdapter.ClearBeforeFill = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Visible = false;
            // 
            // darokhaneDataSet1
            // 
            this.darokhaneDataSet1.DataSetName = "darokhaneDataSet1";
            this.darokhaneDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dragGBindingSource1
            // 
            this.dragGBindingSource1.DataMember = "dragG";
            this.dragGBindingSource1.DataSource = this.darokhaneDataSet1;
            // 
            // dragGTableAdapter1
            // 
            this.dragGTableAdapter1.ClearBeforeFill = true;
            // 
            // drag_g
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1183, 606);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "drag_g";
            this.Text = "drag_g";
            this.Load += new System.EventHandler(this.drag_g_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.darokhaneDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.darokhaneDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dragGBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private darokhaneDataSet darokhaneDataSet;
        private System.Windows.Forms.BindingSource dragGBindingSource;
        private darokhaneDataSetTableAdapters.dragGTableAdapter dragGTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dragIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dragNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dragSEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dragGDataGridViewTextBoxColumn;
        private darokhaneDataSet1 darokhaneDataSet1;
        private System.Windows.Forms.BindingSource dragGBindingSource1;
        private darokhaneDataSet1TableAdapters.dragGTableAdapter dragGTableAdapter1;
    }
}