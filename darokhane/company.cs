﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;
using Microsoft.Office.Interop.Excel;

namespace darokhane
{
    public partial class company : Form
    {
        public company()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses2DataContext();
                int n = Convert.ToInt16(textBox51.Text);
                var d = db.tabs.Where(y => y.facnum == n);
                if (d.Count() != 0)
                {

                    label5.Visible = true;
                    label5.Text = "دارو موجود است";



                }


                else if (d.Count() == 0)
                {

                    label5.Visible = false;
                    label5.Text = "";



                }
                else
                {
                    label5.Visible = false;
                    label5.Text = "";
                }
            }
            catch { }
        }
       // string a, b, c, d, s, f, g,h;

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses2DataContext();
                int n = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                var ed = db.tabs.Where(y => y.facnum == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                ed.copmpanyName = textBox52.Text;
                ed.facdata = textBox53.Text;
                ed.dragname = textBox55.Text;
                ed.count = textBox56.Text;
                ed.exp = textBox57.Text;
                ed.cost = textBox58.Text;
                ed.costmull = textBox59.Text;
                db.SubmitChanges();
                dataGridView1.DataSource = db.tabs;
            }
            catch {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var db = new DataClasses2DataContext();
            var t = db.tabs;
            dataGridView1.DataSource = t;





            //     a=textBox51.Text;
            //     b=textBox52.Text;
            //     c=textBox53.Text;
            //    d=textBox55.Text;
            //    s=textBox56.Text;
            //    f=textBox57.Text;
            //    g = textBox58.Text;
            //    h = textBox59.Text;

            //    dataGridView1.Rows.Add(a,b,c,d,s,f,g,h);
            //   textBox55.Text = "";
            //    textBox56.Text= "";
            //    textBox57.Text = "";
            //    textBox58.Text = "";
            //    textBox59.Text = "";
            //    textBox55.Focus();
        }
        int sumall = 0;
        int sum = 0;
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses2DataContext();
                tab t = new tab() {
                    facnum = Convert.ToInt16(textBox51.Text),
                    copmpanyName = textBox52.Text,
                    facdata = textBox53.Text,
                    dragname = textBox55.Text,
                    count = textBox56.Text,
                    exp = textBox57.Text,
                    cost = textBox58.Text,
                    costmull = textBox59.Text

                };
                db.tabs.InsertOnSubmit(t);
                db.SubmitChanges();
                dataGridView1.DataSource = db.tabs;
                sumall += int.Parse(textBox59.Text) * int.Parse(textBox56.Text);
                sum += int.Parse(textBox58.Text) * int.Parse(textBox56.Text);
                textBox8.Text = Convert.ToString(sumall);
                textBox9.Text = Convert.ToString(sum);
                textBox51.Text = "";
                textBox52.Text = "";
                textBox53.Text = "";
                
                textBox55.Text = "";
                textBox56.Text = "";
                textBox57.Text = "";
                textBox58.Text = "";
                textBox59.Text = "";
                textBox53.Focus();
                // dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
            }
            catch {
                MessageBox.Show("ثبت شد");
            
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses2DataContext();
                int n = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                var d = db.tabs.Where(y => y.facnum == n);
                if (d.Count() != 0)
                {
                    db.tabs.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();
                    dataGridView1.DataSource = db.tabs;
                    
                    


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }    
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var db = new DataClasses2DataContext();
            var serch = db.tabs.Where(c=>c.dragname.Contains(textBox1.Text));
            dataGridView1.DataSource = serch;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox51.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox52.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox53.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
          //  textBox54.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox55.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox56.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox57.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            textBox58.Text = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            textBox59.Text = dataGridView1.CurrentRow.Cells[7].Value.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox51.Text = "";
            textBox52.Text = "";
            textBox53.Text = "";
           // textBox54.Text = "";
            textBox55.Text = "";
            textBox56.Text = "";
            textBox57.Text = "";
            textBox58.Text = "";
            textBox59.Text = "";
        }
        //string filename;
        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.DefaultExt = "sace execl";
                saveFileDialog1.Filter = "excelfile(*.xlsx)|*.xlsx|exel2003(*.xls)|*.xls";
                saveFileDialog1.OverwritePrompt = true;
                saveFileDialog1.Title = "back up to Exel file";
                saveFileDialog1.FileName = "";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //     filename = saveFileDialog1.FileName;
                    //   System.IO.File.WriteAllText(filename,dataGridView1.DataSource);
                    Microsoft.Office.Interop.Excel.Application exelapp = new Microsoft.Office.Interop.Excel.Application();
                    exelapp.Application.Workbooks.Add(Type.Missing);
                    exelapp.Columns.ColumnWidth = 20;
                    for (int i = 1; i < dataGridView1.Columns.Count; i++)
                    {
                        exelapp.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;


                    }
                    for (int x = 0; x < dataGridView1.Rows.Count; x++)
                    {
                        for (int i = 0; i < dataGridView1.Columns.Count; i++)
                        {
                            exelapp.Cells[x + 2, i + 1] = dataGridView1.Rows[x].Cells[i].Value.ToString();



                        }



                    }
                    exelapp.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName.ToString());
                    exelapp.ActiveWorkbook.Saved = true;
                    exelapp.Quit();

                }
            }
            catch { } }

        private void button4_Click_2(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("company.frx");
            re.Show();
        }
    }
}
