﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace darokhane
{
    public partial class drag_T : Form
    {
        public drag_T()
        {
            InitializeComponent();
        }

        private void drag_T_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'darokhaneDataSet.drag' table. You can move, or remove it, as needed.
            this.dragTableAdapter.Fill(this.darokhaneDataSet.drag);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = new DataClasses4DataContext();
            var t = db.@drags;
            dataGridView1.DataSource = t;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var db = new DataClasses4DataContext();
           // var name = dataGridView1.CurrentRow.Cells[1].ToString();
            var serch = db.drags.Where(s=>s.dragName.Contains(textBox2.Text));
            dataGridView1.DataSource = serch;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses4DataContext();
               // var name = dataGridView1.CurrentRow.Cells[2].ToString();
                var serch = db.drags.Where(s => s.dragType.Contains(textBox3.Text));
                dataGridView1.DataSource = serch;
            }
            catch {
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var db = new DataClasses4DataContext();
            //    var name = dataGridView1.CurrentRow.Cells[3].ToString();
                var serch = db.drags.Where(s => s.cat.Contains(textBox4.Text));
                dataGridView1.DataSource = serch;
            }
            catch { }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
