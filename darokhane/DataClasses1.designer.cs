﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace darokhane
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="darokhane")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void Inserttable(table instance);
    partial void Updatetable(table instance);
    partial void Deletetable(table instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::darokhane.Properties.Settings.Default.darokhaneConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<table> tables
		{
			get
			{
				return this.GetTable<table>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.bemeh3")]
	public partial class table : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _facid;
		
		private string _dacdata;
		
		private string _fname;
		
		private string _lname;
		
		private string _phone;
		
		private string _cod;
		
		private string _typebemeh;
		
		private string _bemehpage;
		
		private int _dragid;
		
		private string _dragname;
		
		private System.Nullable<int> _dragcount;
		
		private System.Nullable<int> _dragcost;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnfacidChanging(string value);
    partial void OnfacidChanged();
    partial void OndacdataChanging(string value);
    partial void OndacdataChanged();
    partial void OnfnameChanging(string value);
    partial void OnfnameChanged();
    partial void OnlnameChanging(string value);
    partial void OnlnameChanged();
    partial void OnphoneChanging(string value);
    partial void OnphoneChanged();
    partial void OncodChanging(string value);
    partial void OncodChanged();
    partial void OntypebemehChanging(string value);
    partial void OntypebemehChanged();
    partial void OnbemehpageChanging(string value);
    partial void OnbemehpageChanged();
    partial void OndragidChanging(int value);
    partial void OndragidChanged();
    partial void OndragnameChanging(string value);
    partial void OndragnameChanged();
    partial void OndragcountChanging(System.Nullable<int> value);
    partial void OndragcountChanged();
    partial void OndragcostChanging(System.Nullable<int> value);
    partial void OndragcostChanged();
    #endregion
		
		public table()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_facid", DbType="NChar(100)")]
		public string facid
		{
			get
			{
				return this._facid;
			}
			set
			{
				if ((this._facid != value))
				{
					this.OnfacidChanging(value);
					this.SendPropertyChanging();
					this._facid = value;
					this.SendPropertyChanged("facid");
					this.OnfacidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dacdata", DbType="NChar(100)")]
		public string dacdata
		{
			get
			{
				return this._dacdata;
			}
			set
			{
				if ((this._dacdata != value))
				{
					this.OndacdataChanging(value);
					this.SendPropertyChanging();
					this._dacdata = value;
					this.SendPropertyChanged("dacdata");
					this.OndacdataChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_fname", DbType="NChar(100)")]
		public string fname
		{
			get
			{
				return this._fname;
			}
			set
			{
				if ((this._fname != value))
				{
					this.OnfnameChanging(value);
					this.SendPropertyChanging();
					this._fname = value;
					this.SendPropertyChanged("fname");
					this.OnfnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_lname", DbType="NChar(100)")]
		public string lname
		{
			get
			{
				return this._lname;
			}
			set
			{
				if ((this._lname != value))
				{
					this.OnlnameChanging(value);
					this.SendPropertyChanging();
					this._lname = value;
					this.SendPropertyChanged("lname");
					this.OnlnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_phone", DbType="NChar(100)")]
		public string phone
		{
			get
			{
				return this._phone;
			}
			set
			{
				if ((this._phone != value))
				{
					this.OnphoneChanging(value);
					this.SendPropertyChanging();
					this._phone = value;
					this.SendPropertyChanged("phone");
					this.OnphoneChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_cod", DbType="NChar(100)")]
		public string cod
		{
			get
			{
				return this._cod;
			}
			set
			{
				if ((this._cod != value))
				{
					this.OncodChanging(value);
					this.SendPropertyChanging();
					this._cod = value;
					this.SendPropertyChanged("cod");
					this.OncodChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_typebemeh", DbType="NChar(100)")]
		public string typebemeh
		{
			get
			{
				return this._typebemeh;
			}
			set
			{
				if ((this._typebemeh != value))
				{
					this.OntypebemehChanging(value);
					this.SendPropertyChanging();
					this._typebemeh = value;
					this.SendPropertyChanged("typebemeh");
					this.OntypebemehChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_bemehpage", DbType="NChar(100)")]
		public string bemehpage
		{
			get
			{
				return this._bemehpage;
			}
			set
			{
				if ((this._bemehpage != value))
				{
					this.OnbemehpageChanging(value);
					this.SendPropertyChanging();
					this._bemehpage = value;
					this.SendPropertyChanged("bemehpage");
					this.OnbemehpageChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dragid", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int dragid
		{
			get
			{
				return this._dragid;
			}
			set
			{
				if ((this._dragid != value))
				{
					this.OndragidChanging(value);
					this.SendPropertyChanging();
					this._dragid = value;
					this.SendPropertyChanged("dragid");
					this.OndragidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dragname", DbType="NChar(100)")]
		public string dragname
		{
			get
			{
				return this._dragname;
			}
			set
			{
				if ((this._dragname != value))
				{
					this.OndragnameChanging(value);
					this.SendPropertyChanging();
					this._dragname = value;
					this.SendPropertyChanged("dragname");
					this.OndragnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dragcount", DbType="Int")]
		public System.Nullable<int> dragcount
		{
			get
			{
				return this._dragcount;
			}
			set
			{
				if ((this._dragcount != value))
				{
					this.OndragcountChanging(value);
					this.SendPropertyChanging();
					this._dragcount = value;
					this.SendPropertyChanged("dragcount");
					this.OndragcountChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dragcost", DbType="Int")]
		public System.Nullable<int> dragcost
		{
			get
			{
				return this._dragcost;
			}
			set
			{
				if ((this._dragcost != value))
				{
					this.OndragcostChanging(value);
					this.SendPropertyChanging();
					this._dragcost = value;
					this.SendPropertyChanged("dragcost");
					this.OndragcostChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
